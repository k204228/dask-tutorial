{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/dask_horizontal.svg\" align=\"right\" width=\"30%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lazy execution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we discuss some of the concepts behind dask, and lazy execution of code. You do not need to go through this material if you are eager to get on with the tutorial, but it may help understand the concepts underlying dask, how these things fit in with techniques you might already be using, and how to understand things that can go wrong."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prelude"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As Python programmers, you probably already perform certain *tricks* to enable computation of larger-than-memory datasets, parallel execution or delayed/background execution. Perhaps with this phrasing, it is not clear what we mean, but a few examples should make things clearer. The point of Dask is to make simple things easy and complex things possible!\n",
    "\n",
    "Aside from the [detailed introduction](http://dask.pydata.org/en/latest/), we can summarize the basics of Dask as follows:\n",
    "\n",
    "- process data that doesn't fit into memory by breaking it into blocks and specifying task chains\n",
    "- parallelize execution of tasks across cores and even nodes of a cluster\n",
    "- move computation to the data rather than the other way around, to minimize communication overhead\n",
    "\n",
    "All of this allows you to get the most out of your computation resources, but program in a way that is very familiar: for-loops to build basic tasks, Python iterators, and the NumPy (array) and Pandas (dataframe) functions for multi-dimensional or tabular data, respectively.\n",
    "\n",
    "The remainder of this notebook will take you through the first of these programming paradigms. This is more detail than some users will want, who can skip ahead to the iterator, array and dataframe sections; but there will be some data processing tasks that don't easily fit into those abstractions and need to fall back to the methods here.\n",
    "\n",
    "We include a few examples at the end of the notebooks showing that the ideas behind how Dask is built are not actually that novel, and experienced programmers will have met parts of the design in other situations before. Those examples are left for the interested."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dask is a graph execution engine"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dask allows you to construct a prescription for the calculation you want to carry out. That may sound strange, but a simple example will demonstrate that you can achieve this while programming with perfectly ordinary Python functions and for-loops. We saw this in Chapter 02."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dask import delayed\n",
    "\n",
    "@delayed\n",
    "def inc(x):\n",
    "    return x + 1\n",
    "\n",
    "@delayed\n",
    "def add(x, y):\n",
    "    return x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have used the delayed annotation to show that we want these functions to operate lazily — to save the set of inputs and execute only on demand. `dask.delayed` is also a function which can do this, without the annotation, leaving the original function unchanged, e.g.,\n",
    "```python\n",
    "    delayed_inc = delayed(inc)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this looks like ordinary code\n",
    "x = inc(15)\n",
    "y = inc(30)\n",
    "total = add(x, y)\n",
    "# incx, incy and total are all delayed objects. \n",
    "# They contain a prescription of how to execute"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calling a delayed function created a delayed object (`incx, incy, total`) - examine these interactively. Making these objects is somewhat equivalent to constructs like the `lambda` or function wrappers (see below). Each holds a simple dictionary describing the task graph, a full specification of how to carry out the computation.\n",
    "\n",
    "We can visualize the chain of calculations that the object `total` corresponds to as follows; the circles are functions, rectangles are data/results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAIYAAAEUCAYAAADuoE5tAAAAAXNSR0IArs4c6QAAAERlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAhqADAAQAAAABAAABFAAAAAAAYOEKAAAZAklEQVR4Ae1dB7AURRNuJIPkHCQoGUHQkiDgDyoiwQAiUAVqlYqCUCImwFSilCWCKEIRDYigaKmoKDkjSBZBctJCkmSJgjD/fCN7de/ece92b253Zna66r2729DT8823sxN6erIxxt4nKxaBGASycWKwmGP2p0WArrIYWATiIZAj3kGVjh05ckQlc6TZUqxYMWm60qFIaWLMnDmTWrduTUWLFk1H3gPTefToUZo+fbrIW2BGZJGw0m2MgQMH0qVLlwifJokO+bJtDJMYJzEvlhgSwTRJlSWGSaUpMS+WGBLBNEmVJYZJpSkxL5YYEsE0SZUlhkmlKTEvlhgSwTRJlSWGSaUpMS+WGBLBNEmVJYZJpSkxL5YYEsE0SZUlhkmlKTEvlhgSwTRJlSWGSaUpMS+WGBLBNEmVJYZJpSkxL5YYEsE0SZUlhkmlKTEvlhgSwTRJlSWGSaUpMS+WGBLBNEmVJYZJpSkxL5YYEsE0SZUlhkmlKTEvlhgSwTRJlSWGSaUpMS+WGBLBNEmVJYZJpSkxL5YYEsE0SZUlhkmlKTEvlhgSwTRJlSWGSaUpMS9Kh1pCPhGDy7Q4XBcuXKDs2bNLLEb5qpQOtbRy5Upq2LChLzG4zp49K9DNmzevfJRjNCIG14oVK6hBgwYxZ9T5qTQx/ISpSJEiIrljx475mayyadk2Bi+aNWvWEKp3/OG7FbIBYEGCCRMm0Llz58Tfxx9/bHnBEQj9qwThIhGM9fjx44IQhQsXFo3dq64Kd2Ua7txzKixevJguXrwYqSXwfdGiRZHfYf0SemJ89NFHdOrUqUj54zuOhV1C/SpBY7NQoULkdFUdMqDLeuLECcqZM6dzKHSfoa4xEKs8R47MY3w4hnNhllATY/z48XTy5MlM5Y9j48aNy3Q8TAdC/SrJli0b5c6dm/CJ3gkEvRHs7fPPP/+Iv1y5coWJD5G8Zq5HI6fM/3L69GnCH2TIkCGCHP369RO/8+fPT2ElBQAINTHy5ctH+IOACKg1SpQoIX6H/V+o2xhhL/xE+bfESIROiM9ZYoS48BNl3RIjETohPmeJcbnw0WUN+8RZ9HNgiXEZDYxdOGMZ0QCF9bslRlhLPot8W2JkAVBYT1tihLXks8i3JUYWAIX1tCXG5ZJHjwQ9Eyv/IWCJcZkJ6JGgZ2LFEsNyIAECtsZIAE6YT1lihLn0E+TdEiMBOGE+ZYkR5tJPkHdLjMvg2Em0jCyxxLiMh51Es8TIiID9FRcBW2NchsWOfGbkhyXGZTzsyKclRkYE7K+4CIRuJdqZM2do6dKltGTJEtq2bRtt3bpVxMaIjo+BGBnVqlWj6tWrU7NmzahJkyaR9SdxUTTwYCiIgVXt3333HU2aNInmzZtH9erVo9tvv52qVq1KtWvXFiveCxQoIIoX61ax0n3Tpk2COLh+3bp14vpu3brRvffeG45V8LybZqzw8Els+PDhrFy5cuyOO+5gPIwS4zWG6/ziHtwLHdAFndBtsmCq2Uj54Ycf2LXXXss6dOjAVq9eLS2P0AWd0I00TBXjiMFfBax79+6MvyYYj3GRtnKDbqSBtJCmaWJUd/XPP/+kxo0bixXs69evp1atWqWtWQjdSAOr5ZHm3r1705ZWIIpNYTrvVcD9ig0aNMj3LI0aNYpdc8017MCBA76nna4EjXiV/P7774yHL2DDhg1LF05Z6n3//fcFMffv35/ltTpcoD0xeAB6xscc2MiRIwPHu2/fvqLdAZt0F+3HMXgPgXg1TrwLGcirODbRPn360J49e+ibb76JPaXVb60bn6NHjyZedRN/hSgDOmyBTbBNZ9G2xjh8+DDVqlVLjGTWqVNHqTL47bffqEWLFrR582YqXry4UrYla4y2xHjyyScJw9iDBw9ONq++Xte/f38xtK5rzaElMXi3kGrWrCnmMlQNpnbo0CExEYdao3Tp0r6SUkZiWrYxRowYQY888ojSEfZAWNgIW3UU7WoM3g2kSpUq0bRp06hu3bpKY/7rr7+K2djdu3drty5WuxoDvhRo0KlOCjD2hhtuEHuhwGbdRDtizJ8/X/hGpAto+GFcSRKdu9I98PuAzbqJdsRYuHAhcb8I6ThjAg56r7/++ky6E53LdHHMgZYtW9KCBQtijqr/UztiYIwgHeMW5cuXJz6FHnc/1ETnsipiEG3jxo1ZXabcea2IAZc77ApQpkwZT0BiQzw0XuPJv//+K1z24oV0THQunq7oY7AVNsN2nUSrIPP79u2jsmXLusYXr4Jnn32WuFseff/99/TOO++I3gIUzZo1i95++20qVaoUoRcRHVUn0Tk3RsBm2I7dlLQRnWYB+e7G7Oabb3Zt8lNPPcV4I1Dc99Zbb7Fbb71VfMd0fZ48eRif9BK/n3jiCcb3L8nynLjAxT/YDNt1Eq1eJdi77Oqrr3b90PXs2ZMGDhwovKxWrVpFu3btEjq4g43wFEcbAnLbbbdFaoxE58TFLv7B5th911zcHsilWr1KChYsSF622K5cuTK9++67AmCsFfn555/Fd3Q/8XpxBHuhOa+SROec65P9hM2wXSfRqsbAk+fsSOQG5GeeeYYOHjxIY8eOpYoVK0ZuxQgqr+LjNkgTnYsoSPILbPZS0yWpPi2XaUUMFNYff/wh9mB3gwYWDf3999+EVWgYU0DvBAuL7r//flEDTZ48WajbsGGD6EHAsTfROTdpY7ETbIbtWolODSLYWqVKFcaXFboym0/NM/6aEC6Ab775JuN7nbH27dsz3g1lDz30EONdVFahQgXWoEEDxtsb7OWXX054zk3isBU26ybaTaJ17dpVNBIfffRRVw8gqnPsewZBjcF7I5H7UZtgYzxeeGJXxeixjETnIgoSfMGuz3PnzqXPPvsswVXqndLqVQL44BnlZYjZIQV0RJMCv9EwxDHs0BxNiqzO4XxWgnkS9HZ0E+1qDAxW1a9fX3Q9Vd/+Eu0LjHxiUbTTJdaFINrVGAAY09kYwVRdYCNs1Y0UwFU7YsBotC908KXEIJnbthDyp4JoSYxOnToRH86mn376SQUM49oA22Bj586d455X/qBu3SjH3gkTJrBbbrmF8dhZziGlPvlCZwYbdRUtaww8bXz8QQxfjx8/XrmHb9y4caJ3Axu1FV0ZDbu5A4xYzOx2wCudeYYtJUuWZNyhKJ3JpF03BnW0ljFjxjDuGOwphJLsjCMkE2zhczKyVfuuT6vZ1XjVMvehILj7wQkGo5Sxg1fx7knHMYymwr/zf//7Hz3++OPpSMJXndoNcF0JnS5duhBWqMHrijvbXOmytBwHKTBqCpLyGiwtafitVNvGZyxQn3/+uVgS2Lx5c8LoqF+CtDBMD0diU0gB7IwhBhxs0Bto164d3XjjjaLmSDc5UDshLaSJtI0S31s1PiS4bNkydt111zHuU8G4b4X0FKETupEG0jJRjKkxop9WRNGD0w3Wn2CuAu9+rB9NVaADurA8ErqRBtIyUkxke3SeuCMwFpIwvjxAjJQiwh4PTRB9ScLvuBb3YJQVOqALOk0XY3ol8Z5aeIfzYWkxu4k4FWgTfPXVV8KfA1PiTixxBJWPjiWOgPNYPbZ9+3axCAmNy44dO4q4oYjLgRhbCHGASTJTxVhigBSffvqpcB7G+MLs2bMzlCEK19l9AF7c8AGFgCBFihQROw9gBwIEfosWBH6Frnz58tHDDz9sLjlMrBJ79OjBuMeWqPZ5oUqt+p1XE/RycjAe8slECJlxjU/E5po4cWJkmQG8vLgzbvRDn9J36HI8x+B1jldVr169UtKp4s1GEcMhBQrMEQyRy/SgwqslemTVIUfv3r2dJI34NIIYvC4nhxSxC5Iw8BXbTkil5ECyWIdhkIPvZ0IgB2wxQYwgBoKufvvtt5HXR3TBoPchkxjQBZ2xAnJ88cUXNHTo0NhTev42oeUELy6saOevDfHHSyLS8MRiIiwskiXQBZ3RaTjpwgZVPcrc5l97f4zoDCO4O1aYIZQBn+0Uhce7ntGXSPkOnSAG0kBaSNOEwPLR4BjxKnHqakSuwQKfLVu20EsvvSR6D3w+wzkt7RM60TNBGkgLaSJtk8SoAS6saoc4QefhJ3H06FFPUXgSFTKi4xQtWjTiFBSbbqJ7dTlnDDF4VU54kjH07TVGl9dCw24DGCrfuXOniOvpVY9K9xnzKkFgFHhx+U0KFCbSRNpOcBaVCtirLUbUGPD1RPyJtWvXik+vYKRyHxYXwWkHn7pFz4mXbyNqDL7tlfCiAjmCEqQNTy7YYoJoX2Mg6BlibC1atEjMiAZZKJit5REBhVMQnIN1Fu1rDMTVatq0aeCkAAkwTd+sWTMjnIK1rjHOnz8vagu+lbaImaHCE4pYGG3atBFtDWcWVgW73NqgdY2Biat69eopQwqA79gD27SW6GFQnb5jzoKHZlTSSxue47BN5hyN32WjbY2BYGdodKropQ2bYJtuAdky1HB+M1FGepjB5BF+GY+GJ0NdWnTMmTOH1ahRQ9vZVi1rDOyCjEXM2D1IVcGmOHAsnjp1qqomJrYrLY9LmpXyEUbGN8tLcyqpq+fB2dhNN92UuqIANGhXY8yYMUN4ULVt2zYx4xU4i5FQTMfPnDlTAWtcmhAAGVNKEivCpkyZkpIOP2/mq/BZkyZN/ExSSlpa1RhLliwh7OmOqH26CKL2/fXXXwTbdRKtiPHGG29Qv379InuK6AA0vNQHDBhAr7/+ug7mRmzUZkh89erVxH0rxe5EOXPmjGRAhy98oEuMa6A3xbfB0sFkfQKnDBo0SNQWupECLMDOSf379yfkQRfRosbAynOMC8AJJnoVmC4gw070TjAayge+qHbt2sqbrkUbA08aHG51JQVYANv79u2rTa2hfI2xY8cOatSokagtdNtXLLZaOHXqlHA9XL58udSF1rHpyPitfI3Bt62iPn36aLfZXLzCAbGRF+RJdVG6xkBwE8TQgls+gpmYIAjSgmUO2BVa5ppa2dgoXWNga23EzzSFFCg85AV5Qt5Ulkw1xtdff62EvXgf8+22RTdPRtsC22XKEBn4yM6bjHzF4pOBGIhsi9hVHTp0kJGWMjowsIRAashbKhImfDIEmcduxq+++qrYBz0VAFW7F/u6I854qhImfJRuY6RakPZ+7whYYnjHzug7LTGMLl7vmbPE8I6d0XdaYhhdvN4zZ4nhHTuj77TEMLp4vWfOEsM7dkbfaYlhdPF6z5wlhnfsjL7TEsPo4vWeOUsM79gZfaclhtHF6z1zlhjesTP6TksMo4vXe+YsMbxjZ/SdlhhGF6/3zFlieMfO6DstMYwuXu+Zs8Twjp3Rd1piGF283jNnieEdO6PvtMQwuni9Z84Swzt2Rt9piWF08XrPnCWGd+yMvtMSw+ji9Z45Swzv2Bl9pyWG0cXrPXOWGN6xM/pOSwyji9d75iwxvGNn9J2WGEYXr/fMZYioAzWIMVW3bl3vGhW889ChQ1SsWDEploUFnwwxuLB9AoJ0lSxZUgqIiZRs375dnK5atWqiy6ScQ75QoKnmK0z4ZCCGlFJIUokTohFxL61kRiBofAJpY6xdu1ZsX3XhwgXCdysZEVABn0CI8cknn4ho/IjIP2HChIyo2F+kAj6+v0r4hl1UtGhROn78uKBA4cKF6ejRo1rtWpRO7qqCj+81BvYGw44/jly8eJEWL17s/Az9pyr4+E6MDz/8kM6cORMhwOnTpwnHrPyHgCr4+PoqQWMTrW2QIVry5csnXi06bmsVnY9Uv6uEj681xuzZs+mqqzInmT17dpo1a1aquGp/v0r4ZC6lNMI7fvx4OnnyZKYUcAznwi4q4ePrqwR7kObJk0f0QND6huAYvp87d47Onz9PYX6dqIRPprmSdD61qBmwVwdk6NChghDPP/+8+I09ScJMCoCgEj6+EgOF72xKU6BAAbp06RKVLl1aEMP+I4GNKvj42sawha8PApYY+pSVr5ZaYvgKtz6JWWLoU1a+WhoYMdA1izfY5WvuFU4saHwCIwbGLtArsRIfgaDxCYwY8eGwR1VBwBJDlZJQzA5LDMUKRBVzLDFUKQnF7AiMGEG3uhUrh0zmBI1PYMQIutWdqSQUOxA0PoERQ7FysObEIGCJEQOI/fkfApYYlglxEbDEiAuLPWiJYTkQF4HAiBF0dywuGgodDBqfwIgRdHdMIQ7ENSVofAIjRlw07EFlEAiMGPDFQHVpJT4CQeMTGDHgi4Hq0kp8BILGx9flA4AAIQ8Qsmjv3r3CUWfLli0iBBJCI1hRB5+0r0RbsWKFWJe6YMEC2rBhgyADYmFhRRoEK9BAFFSdderUoRYtWlCrVq2oYcOGoeCJqvikhRhYUTVmzBj64IMPKEeOHNS2bVtq2bKliAZYqlSpuAV+8OBBWr9+Pc2ZM4d+/PFHEUPjscceox49ehAWJ5kkWuDD3/PShIdOYkOGDGElSpRg3bp1Y8uXL/esG/dCR/HixYVO6NZddMIHDUAp8ssvv7BatWqxu+66i+3YsUOKTijZuXOn0AndSENX0Q0fKcQYNWqUeLJ5LM20lRt0o/ZAWrqJjvikTIxhw4YxHiWH8YCuaS8vpFGzZk32yiuvpD0tWQnoik9KxHjxxRcFKQ4fPiwLxyz18EYq4+Gf2eDBg7O8NugLdMbHMzFGjhwp2hQHDhzwHX8QkY+aMtigquiOjydirFq1Srzvd+/eHVi57Nq1S9gAW1QT2ISeGWwMSlLFxzUxeChGVqVKFTZt2rSg8hxJFzbAFtikipiCj+u5EoRIwghlu3btAh9zgg2wBTapIsbg4+ZJw7sdDT8+z+HmtrReC1tgk58N4CtlyCR8XNUYI0aMoI4dO1LZsmVVeUCFLbAJtgUtRuFzJfbHHucxv1mZMmXY5s2bY08F/hs2wTbYGJSYhk/SNcb8+fOpQoUKVKNGjaAfzEzpw6ZKlSrRvHnzMp3z64Bp+CRNjOnTp9M999zjCmfMIm7bts3VPV4vvvvuuwk2BiXG4ZNs1VuvXj22cuXKZC8X1w0YMED0513d5PFi2AYbgxLT8EnKHwP7i+TPn19Ers2VK1fSDyV8LPhACzVu3Djpe7xeiHDT8NvAzgbwAfFTTMQnqVcJH+EU7Qs3pEDBwCmnUaNGGcqIP9Hi95Xib13peAYlcX7ANrSBYKvfYiI+SRFj//79xFv9rvDes2cPIU545cqVxX3Lli0TbZQOHTpQ9+7dxdPdpk2byG5HqF3QTnjggQeoQYMGNGnSJFfp4WLYCFv9FhPxSYoYaES6da9DTOy8efMSn2QT5YRNfqFn7ty51LVrV5oyZQrNmDGDQBjUIvfddx917txZ7I9655130nPPPee6fGEj0vBbTMQnqZcxqne3OwNgJ6Nq1apFyghEQe2BtSTNmzcXTsFoC8BbHOTAlpLt27cX1/NGqxhIi9yc5BfY6PVVlGQScS8zEZ+kagw8ic6uh3GR8XAQXuHY2Qi1BR+goty5c5PThkFDl7fyXWuFjW5rNteJxLnBRHySIgb2RT9y5EgcSOQcQu2C6hg7CDqybt0612SEjbDVbzERn6SIgVcCBqqwFaYbwW6J2AAOfxBswOvowDF08/DZtGlTsVdHnz59aOnSpTR58mTi3uaEPVmTFeiFjdGvr2TvTfU6I/FJdkAIfg+bNm1K9nK2Zs0axnsX6Juyp59+mvF2BOOb1jDe1mB83Qh77bXXxDlOCnbo0CE2depU4SaI6+vXr894FzDptHAhbIONQYlp+CTtqMO7mGz48OFpxZ3XIAw+nV4EtsHGoMQ0fJJ6laCqxUqydG+BicYoli96EdgGG4MS4/BJ9gk7e/asqOpVctJxbIdNWMIAG4MS0/BJusbAIuQuXbrQ2LFjg3oor5juuHHjxOCYs1D6ihem8YRx+Lh5wtAg5OEKGB8vcHNbWq+FLbDJbWM1HUaZhE9Ss6vRD1rPnj3FYNR7770XfTiw77zHI7rBo0ePDsyG6ISNwcftk8NjWTA+a8r4ELbbW6VfDxtgC2xSRUzBJ+nuajTwGHOoWLFioK8UvEJgA2xRTUzAxxMxUBC9evViPPoNw9iD34I0W7duzXr37u130kmnpzs+rtsYzvuUIyQi5WCOA2GU/PKawjB6s2bNqFy5cvTll18quxOj7vgk3V11COF8YvocIZF4zArhS+GHHwTSgM9GkyZNlCYFMNIdH8/EcDLPA5pQ+fLlxTT5xo0bHd5I/4RuTMXzxcJigg3T9qoLyKEtPkm/NLO4kFfrwiP8hRdekDoCiRFF6MTqcaShq+iGj+fGZ7wCwtD0gw8+KGZREdjk2LFj8S5L6hjuhQ7MyEKnikPxSWUk6iKd8JFKDAcDTIGjMAsWLMj4MDqbOHEiSybACneqFdfiHtwLHW6m+p30Vf/UAR/PvZJk3u8nTpwQjcTZs2fTwoULRQ+CR98T7QTHBQ8NSu6PQRws4a8Jf1A4A3fq1IkKFSqUTDLaXqMyPmklRmyJ7du3j7Zu3SqI4PRiQBA0KKtXr67UKvpY2/34rRI+vhLDD3BtGnIQUL/PJyefVotLBCwxXAIWlsv/DwPvejRrmFQfAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<IPython.core.display.Image object>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "total.visualize()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But so far, no functions have actually been executed. This demonstrated the division between the graph-creation part of Dask (`delayed()`, in this example) and the graph execution part of Dask.\n",
    "\n",
    "To run the \"graph\" in the visualization, and actually get a result, do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "47"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# execute all tasks\n",
    "total.compute()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Why should you care about this?**\n",
    "\n",
    "By building a specification of the calculation we want to carry out before executing anything, we can pass the specification to an *execution engine* for evaluation. In the case of Dask, this execution engine could be running on many nodes of a cluster, so you have access to the full number of CPU cores and memory across all the machines. Dask will intelligently execute your calculation with care for minimizing the amount of data held in memory, while parallelizing over the tasks that make up a graph. Notice that in the animated diagram below, where four workers are processing the (simple) graph, execution progresses vertically up the branches first, so that intermediate results can be expunged before moving onto a new branch.\n",
    "\n",
    "With `delayed` and normal pythonic looped code, very complex graphs can be built up and passed on to Dask for execution. See a nice example of [simulated complex ETL](https://blog.dask.org/2017/01/24/dask-custom) work flow.\n",
    "\n",
    "![this](images/grid_search_schedule.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will apply `delayed` to a real data processing task, albeit a simple one.\n",
    "\n",
    "Consider reading three CSV files with `pd.read_csv` and then measuring their total length. We will consider how you would do this with ordinary Python code, then build a graph for this process using delayed, and finally execute this graph using Dask, for a handy speed-up factor of more than two (there are only three inputs to parallelize over)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Created CSV acccouts in 3.67s\n",
      "Created JSON acccouts in 31.92s\n"
     ]
    }
   ],
   "source": [
    "%run prep.py -d accounts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['data/accounts.0.csv', 'data/accounts.1.csv', 'data/accounts.2.csv']"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import pandas as pd\n",
    "import os\n",
    "filenames = [os.path.join('data', 'accounts.%d.csv' % i) for i in [0, 1, 2]]\n",
    "filenames"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3000000\n",
      "CPU times: user 508 ms, sys: 71.6 ms, total: 580 ms\n",
      "Wall time: 579 ms\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "\n",
    "# normal, sequential code\n",
    "a = pd.read_csv(filenames[0])\n",
    "b = pd.read_csv(filenames[1])\n",
    "c = pd.read_csv(filenames[2])\n",
    "\n",
    "na = len(a)\n",
    "nb = len(b)\n",
    "nc = len(c)\n",
    "\n",
    "total = sum([na, nb, nc])\n",
    "print(total)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your task is to recreate this graph again using the delayed function on the original Python code. The three functions you want to delay are `pd.read_csv`, `len` and `sum`.. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "delayed_read_csv = delayed(pd.read_csv)\n",
    "a = delayed_read_csv(filenames[0])\n",
    "...\n",
    "\n",
    "total = ...\n",
    "\n",
    "# execute\n",
    "%time total.compute()   \n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 549 ms, sys: 118 ms, total: 667 ms\n",
      "Wall time: 316 ms\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "3000000"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%time\n",
    "\n",
    "# your verbose code here\n",
    "delayed_read_csv = delayed(pd.read_csv)\n",
    "a = delayed_read_csv(filenames[0])\n",
    "b = delayed_read_csv(filenames[1])\n",
    "c = delayed_read_csv(filenames[2])\n",
    "\n",
    "na = delayed(len)(a)\n",
    "nb = delayed(len)(b)\n",
    "nc = delayed(len)(c)\n",
    "\n",
    "total = delayed(sum)([na, nb, nc])\n",
    "total.compute()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, repeat this using loops, rather than writing out all the variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 552 ms, sys: 125 ms, total: 677 ms\n",
      "Wall time: 327 ms\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "3000000"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%time\n",
    "\n",
    "# your concise code here\n",
    "results = []\n",
    "delayed_read_csv = delayed(pd.read_csv)\n",
    "\n",
    "for x in filenames:\n",
    "    a = delayed_read_csv(x)\n",
    "    na = delayed(len)(a)\n",
    "    results.append(na)\n",
    "\n",
    "total = delayed(sum)(results)\n",
    "\n",
    "total.compute()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3000000\n",
      "CPU times: user 545 ms, sys: 121 ms, total: 666 ms\n",
      "Wall time: 308 ms\n",
      "3000000\n",
      "CPU times: user 523 ms, sys: 113 ms, total: 635 ms\n",
      "Wall time: 294 ms\n"
     ]
    }
   ],
   "source": [
    "## verbose version\n",
    "delayed_read_csv = delayed(pd.read_csv)\n",
    "a = delayed_read_csv(filenames[0])\n",
    "b = delayed_read_csv(filenames[1])\n",
    "c = delayed_read_csv(filenames[2])\n",
    "\n",
    "delayed_len = delayed(len)\n",
    "na = delayed_len(a)\n",
    "nb = delayed_len(b)\n",
    "nc = delayed_len(c)\n",
    "\n",
    "delayed_sum = delayed(sum)\n",
    "\n",
    "total = delayed_sum([na, nb, nc])\n",
    "%time print(total.compute())\n",
    "\n",
    "\n",
    "## concise version\n",
    "csvs = [delayed(pd.read_csv)(fn) for fn in filenames]\n",
    "lens = [delayed(len)(csv) for csv in csvs]\n",
    "total = delayed(sum)(lens)\n",
    "%time print(total.compute())\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Notes**\n",
    "\n",
    "Delayed objects support various operations:\n",
    "```python\n",
    "    x2 = x + 1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "if `x` was a delayed result (like `total`, above), then so is `x2`. Supported operations include arithmetic operators, item or slice selection, attribute access and method calls - essentially anything that could be phrased as a `lambda` expression.\n",
    "\n",
    "Operations which are *not* supported include mutation, setter methods, iteration (for) and bool (predicate)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Appendix: Further detail and examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following examples show that the kinds of things Dask does are not so far removed from normal Python programming when dealing with big data. These examples are **only meant for experts**, typical users can continue with the next notebook in the tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 1: simple word count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This directory contains a file called `README.md`. How would you count the number of words in that file?\n",
    "\n",
    "The simplest approach would be to load all the data into memory, split on whitespace and count the number of results. Here we use a regular expression to split words."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import re\n",
    "splitter = re.compile('\\w+')\n",
    "with open('README.md', 'r') as f:\n",
    "    data = f.read()\n",
    "result = len(splitter.findall(data))\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trouble with this approach is that it does not scale - if the file is very large, it, and the generated list of words, might fill up memory. We can easily avoid that, because we only need a simple sum, and each line is totally independent of the others. Now we evaluate each piece of data and immediately free up the space again, so we could perform this on arbitrarily-large files. Note that there is often a trade-off between time-efficiency and memory footprint: the following uses very little memory, but may be slower for files that do not fill a large faction of memory. In general, one would like chunks small enough not to stress memory, but big enough for efficient use of the CPU."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = 0\n",
    "with open('README.md', 'r') as f:\n",
    "    for line in f:\n",
    "        result += len(splitter.findall(line))\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 2: background execution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many tasks that take a while to complete, but don't actually require much of the CPU, for example anything that requires communication over a network, or input from a user. In typical sequential programming, execution would need to halt while the process completes, and then continue execution. That would be dreadful for a user experience (imagine the slow progress bar that locks up the application and cannot be canceled), and wasteful of time (the CPU could have been doing useful work in the meantime.\n",
    "\n",
    "For example, we can launch processes and get their output as follows:\n",
    "```python\n",
    "    import subprocess\n",
    "    p = subprocess.Popen(command, stdout=subprocess.PIPE)\n",
    "    p.returncode\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The task is run in a separate process, and the return-code will remain `None` until it completes, when it will change to `0`. To get the result back, we need `out = p.communicate()[0]` (which would block if the process was not complete)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, we can launch Python processes and threads in the background. Some methods allow mapping over multiple inputs and gathering the results, more on that later.  The thread starts and the cell completes immediately, but the data associated with the download only appears in the queue object some time later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Edit sources.py to configure source locations\n",
    "import sources\n",
    "sources.lazy_url"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import threading\n",
    "import queue\n",
    "import urllib\n",
    "\n",
    "def get_webdata(url, q):\n",
    "    u = urllib.request.urlopen(url)\n",
    "    # raise ValueError\n",
    "    q.put(u.read())\n",
    "\n",
    "q = queue.Queue()\n",
    "t = threading.Thread(target=get_webdata, args=(sources.lazy_url, q))\n",
    "t.start()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# fetch result back into this thread. If the worker thread is not done, this would wait.\n",
    "q.get()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider: what would you see if there had been an exception within the `get_webdata` function? You could uncomment the `raise` line, above, and re-execute the two cells. What happens? Is there any way to debug the execution to find the root cause of the error?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 3: delayed execution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many ways in Python to specify the computation you want to execute, but only run it *later*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(x, y):\n",
    "    return x + y\n",
    "\n",
    "# Sometimes we defer computations with strings\n",
    "x = 15\n",
    "y = 30\n",
    "z = \"add(x, y)\"\n",
    "eval(z)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we can use lambda or other \"closure\"\n",
    "x = 15\n",
    "y = 30\n",
    "z = lambda: add(x, y)\n",
    "z()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A very similar thing happens in functools.partial\n",
    "\n",
    "import functools\n",
    "z = functools.partial(add, x, y)\n",
    "z()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python generators are delayed execution by default\n",
    "# Many Python functions expect such iterable objects\n",
    "\n",
    "def gen():\n",
    "    res = x\n",
    "    yield res\n",
    "    res += y\n",
    "    yield y\n",
    "\n",
    "g = gen()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run once: we get one value and execution halts within the generator\n",
    "# run again and the execution completes\n",
    "next(g)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dask graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any Dask object, such as `total`, above, has an attribute which describes the calculations necessary to produce that result. Indeed, this is exactly the graph that we have been talking about, which can be visualized. We see that it is a simple dictionary, the keys are unique task identifiers, and the values are the functions and inputs for calculation.\n",
    "\n",
    "`delayed` is a handy mechanism for creating the Dask graph, but the adventerous may wish to play with the full fexibility afforded by building the graph dictionaries directly. Detailed information can be found [here](http://dask.pydata.org/en/latest/graphs.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total.dask"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dict(total.dask)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
